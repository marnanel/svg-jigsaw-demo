from bs4 import BeautifulSoup

SOLUTION_IMAGE = 'jobs-tormentors.jpg'
OUTPUT_FILENAME = 'jobs-tormentors-jigsaw.svg'

def main():

    with open('die.svg', 'r') as f:
        soup = BeautifulSoup(f, features='xml')

    defs = soup('defs')[0]
    main = soup('svg')[0]

    solution = soup.new_tag('image')
    solution['width'] = main['width']
    solution['height'] = main['height']
    solution['x'] = 0
    solution['y'] = 0
    solution['preserveAspectRatio'] = 'none'
    solution['id'] = 'solution'
    solution['xlink:href'] = SOLUTION_IMAGE

    solution_group = soup.new_tag('g')
    solution_group['inkscape:groupmode'] = 'layer'
    solution_group['inkscape:label'] = 'Solution'
    solution_group['style'] = 'display:none'

    solution_group.append(solution)
    main.append(solution_group)

    for piece in soup('path'):
        name = 'clip-'+piece['id']

        clip_path = soup.new_tag('svg:clipPath')
        clip_path['clipPathUnits']='userSpaceOnUse'
        clip_path['id'] = name

        clip_path.append('\n')

        piece.extract()
        clip_path.append(piece)

        defs.append(clip_path)
        defs.append('\n\n')

        use = soup.new_tag('use')
        use['x'] = 0
        use['y'] = 0
        use['xlink:href'] = '#solution'
        use['clip-path'] = f'url(#{name})'

        main.append(use)

    with open(OUTPUT_FILENAME, 'w') as f:
        f.write(str(soup))

if __name__=='__main__':
     main()
