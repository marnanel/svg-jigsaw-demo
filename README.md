This example converts the supplied image to an SVG jigsaw.
It's part of [https://phabricator.wikimedia.org/T194344](T194344)
on Phabricator.

The image is linked, not embedded. You can convert it to an embedded image
using, for example, Inkscape (Extensions > Images > Embed images).

If you look at the SVG using a simple image viewer, it will appear to be
identical to the source image. But if you load it into an editor, you will
be able to drag the pieces around.

The jigsaw works by cloning the source image once per piece, then
using the pieces as clip paths. The image itself is on another layer,
which is set to be invisible. (You could think of that as the box lid!)

The die is taken from
[File:Jigsaw die](https://commons.wikimedia.org/wiki/File:Jigsaw_die.svg),
which is based on
[File:Jigsaw puzzle](https://commons.wikimedia.org/wiki/File:Jigsaw_puzzle.svg)
by Commons user Heljastrom.
The source image is
*[Job's Tormentors](https://commons.wikimedia.org/wiki/File:William_Blake_Job%27s_Tormentors_c1785-90_this_state_1800-25_British_Museum_London.jpg)*
by William Blake.

# How to build

```
python -m venv venv
./venv/bin/activate
python -m pip install -r requirements.txt
make
```

